import base64
import io
import os
from time import strftime, gmtime

import requests
from PIL import Image, ImageSequence
from PIL import ImageDraw
from flask import Flask, render_template, request, send_file

application = Flask(__name__)

YUBIKEY_ID = os.getenv('YUBIKEY_ID', '').replace('\n', '')
PASSWORD = os.getenv('PASSWORD', 'hello').replace('\n', '')
DURATION = int(os.getenv('DURATION', '50').replace('\n', ''))


def remove_ext(name):
    return '.'.join(name.split('.')[:-1])


def prune_data():
    files = sorted(os.listdir('data'), reverse=True)
    if len(files) > 7:
        for f in files[7:]:  # keep only last week
            os.remove('data/%s' % f)


def get_gif(name):
    data = open('data/%s' % name, 'rb').read()
    return base64.b64encode(data).decode('utf-8')


def auth(token) -> bool:
    if YUBIKEY_ID != '':  # Yubikey token
        if token[:12] != YUBIKEY_ID:
            return False
        resp = requests.post('https://demo.yubico.com/api/v1/simple/otp/validate',
                             json={'key': token}, headers={'Content-Type': 'application/json'}).json()
        return resp['status'] == 'success'
    else:
        return token == PASSWORD


@application.route('/')
def home():
    return render_template('home.html', wrong=False)


@application.route('/image.png')
def image():
    return send_file('image.png')

@application.route('/dashboard')
def dashboard():
    token = request.args.get('token')
    if not auth(token):
        return render_template('home.html', wrong=True)

    prune_data()
    files = os.listdir('data')
    days = [{'date': remove_ext(f), 'data': get_gif(f)} for f in sorted(files, reverse=True)]
    return render_template('dashboard.html', days=days)


@application.route('/update', methods=['POST'])
def update():
    image = Image.open(io.BytesIO(request.data))
    name = strftime("%Y-%m-%d", gmtime())
    path = 'data/%s.gif' % name

    ImageDraw.Draw(image).text((0, 0), strftime("%H:%M:%S", gmtime()), (255, 0, 0))

    if os.path.isfile(path):
        gif = Image.open(path)
        frames = [f.copy() for f in ImageSequence.Iterator(gif)] + [image]
        frames[0].save(path, save_all=True, append_images=frames[1:], duration=DURATION, loop=0)
    else:
        image.save(path, save_all=True, append_images=[], duration=DURATION, loop=0)
    return dict(), 200


if __name__ == '__main__':
    application.run(host='0.0.0.0', port=5000, debug=True)