import base64
import sys
import io
import picamera
import time

import requests


def take_shot():
    stream = io.BytesIO()
    with picamera.PiCamera() as camera:
        camera.resolution = (512, 384)
        camera.start_preview()
        camera.rotation = 180
        time.sleep(2)

        camera.capture(stream, 'jpeg')
    stream.seek(0)
    return stream.read()


if __name__ == '__main__':
    img = take_shot()
    print(len(img))
    data = base64.b64encode(img).decode()
    host = sys.argv[1] if len(sys.argv) == 2 else 'http://localhost:5000'
    requests.post('%s/update' % host, data=img)