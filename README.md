# Rasp-Watcher

Rasp-Watcher is a complete IP-webcam platform build and provide by your self to keep date on your control.

![image](server/image.png)

## Server
Deploy docker image on your server

`docker run -p 8080:8080 -e PASSWORD=**** -e DURATION=100 registry.gitlab.com/fjolain/rasp-watcher:latest`

`DURATION` means the durations between two images when concatenate into a gif.

You can also find kubernetes config files to update with your value at `server/deply/k8s`

## Firmware
Firmware is a python script `firmware/shot.py`. Run `python3 shot.py http://your.host` on a raspberry pi with Camera Pi enable  to take a shot and send it to your server

## Mechanics

You must have a raspberry pi with a official camera and an internet connection to use this project.

If you have a 3D printer you can find a perfect case at `meca`